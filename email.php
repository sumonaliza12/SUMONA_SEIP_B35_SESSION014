<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book Title usingBootstrap</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <form>
        <div class="form-group">
            <label for="text">Person's  Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter the Person's name here..">
        </div>
        <div class="form-group">
            <label for="">Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Enter the email here..">
        </div>

        <button type="button" class="btn btn-warning">Add</button>
        <button type="button" class="btn btn-primary">Add & Save</button>
        <button type="button" class="btn btn-success">Reset</button>
        <button type="button" class="btn btn-info">Back To The List</button>
    </form>
</div>

</body>
</html>

